import pandas as pd
import numpy as np

from matplotlib import pyplot as plt

from sklearn.metrics import confusion_matrix
from collections import Counter
import numpy as np
from sklearn.metrics import precision_recall_curve, roc_auc_score, classification_report
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from catboost import CatBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.pipeline import Pipeline
from sklearn.metrics import cohen_kappa_score
from sklearn.model_selection import StratifiedKFold
import tqdm

def simple_preprocessor(s):
    return s.lower().replace('"', ' ').replace("''", ' ')

def confusion_matrix_with_names(y, p, classes=None):
    """
        Beauty print confusion matrix
    """
    dct = Counter(y)
    if classes is None:
        classes = list(dct.keys())
    _classes = sorted(classes, key = lambda x:-dct[x])
    
    M = confusion_matrix(y, p, labels=_classes).T
    
    N = max([len(i) for i in classes])
    classes_names = [i+" "*(N-len(i))+'|' for i in _classes]
    print('Pred\True'+' '*(N+1-10)+'|'+''.join(classes_names))
    for key, M_line in zip(classes_names, M):
        s = key+'|'.join([str(i)+' '*(N-len(str(i))) for i in M_line])
        print('='*(len(s)))
        print(s)
        
    return


def show_score(y_truth, p_predict, classes, betta = 0.1, print_scores = True):
    """
        Print scores Pr, Rec, F01 by classes
        Return median(F01)
    """
    dct = Counter(y_truth)
    classes_ = list(sorted(classes, key = lambda x:-dct[x]))
    M = confusion_matrix(y_truth, [classes[i] for i in np.argmax(p_predict, axis = 1)], labels = classes_).T
    pr_list  = []
    rec_list = []
    fb_list  = []
    
    for c, cls in enumerate(classes_):
        pr  = M[c, c]/M[c, :].sum()
        rec = M[c, c]/M[:, c].sum()
        fb  = (1+betta**2)*pr*rec/(pr*(betta**2)+rec)
        pr_list.append(pr)
        rec_list.append(rec)
        fb_list.append(fb)
        
    N = max([len(i) for i in classes])
    if print_scores:
        print('classes:  '+' '.join([cl+' '*(N-len(cl)) for cl in classes_]))
        print('Pr:       '+' '.join(["%2.4f"%p+' '*(N-len("%2.4f"%p)) for p in pr_list]))
        print('Rec:      '+' '.join(["%2.4f"%p+' '*(N-len("%2.4f"%p)) for p in rec_list]))
        print('F01:      '+' '.join(["%2.4f"%p+' '*(N-len("%2.4f"%p)) for p in fb_list]))        

    return np.median([i for i in fb_list if ~np.isnan(i)])

def evaluate_model(X, y, model, fold, print_scores=True):
    """
        For fit-predict by every fold with temp results if print_scores==True
    """
    y_truth_list   = []
    y_predict_list = []
    texts_list     = []
    scores         = []
    for train_ind, test_ind in fold.split(X, y):
        model.fit(X[train_ind], y[train_ind])
        y_predict = model.predict_proba(X[test_ind])

        current_score = show_score(y[test_ind], 
                                   y_predict, 
                                   classes = model.classes_,
                                   print_scores = print_scores)

        scores.append(current_score)
        y_truth_list.extend(y[test_ind])
        y_predict_list.extend([model.classes_[i] for i in np.argmax(y_predict, axis = 1)])
        texts_list.extend(X[test_ind])
       
    print("\nfold mean (class median (F01)) %2.4f\n\n"%np.mean(scores))
    return y_truth_list, y_predict_list, texts_list


def get_features_list(C, X, y, fold = None):
    """
        return used features
    """
    if fold is None:
        fold = StratifiedKFold(n_splits = 4, random_state=42)
    model_for_feature_selection = model_M1 = Pipeline([('Tf-IDF', TfidfVectorizer(lowercase=True, 
                                                         preprocessor=lambda x:x.replace('"', ' ').replace("''", ' '),
                                                         ngram_range=(1,4), max_features=10000)),
                                                         ('LR(l1)', LogisticRegression('l1',
                                                                                       multi_class = 'auto',
                                                                                       solver = 'liblinear',
                                                                                       C=C))])

    used_features = []
    y_truth_list   = []
    y_predict_list = []
    texts_list     = []
    scores         = []
    for train_ind, test_ind in fold.split(X, y):
        model_for_feature_selection.fit(X[train_ind], y[train_ind])
        y_predict = model_for_feature_selection.predict_proba(X[test_ind])

        current_score = show_score(y[test_ind], 
                                   y_predict, 
                                   classes = model_for_feature_selection.classes_,
                                   print_scores = False)

        scores.append(current_score)
        y_truth_list.extend(y[test_ind])
        y_predict_list.extend([model_for_feature_selection.classes_[i] for i in np.argmax(y_predict, axis = 1)])
        texts_list.extend(X[test_ind])

        dct = {v:k for k,v in model_for_feature_selection.steps[0][1].vocabulary_.items()}
        ind_list = np.where(model_for_feature_selection.steps[1][1].coef_.sum(axis = 0)!=0)[0]
        features = [dct[i] for i in ind_list]
        used_features.extend(features)
    return used_features
