import pickle
from utils import simple_preprocessor
import numpy as np
import pandas as pd

def simple_preprocessor(s):
    return s.lower().replace('"', ' ').replace("''", ' ')


model_M1 = pickle.load(open('model_M1.pickle', 'rb'))
model_M2 = pickle.load(open('model_M2.pickle', 'rb'))
rename_dict_inverse = {'NOT': 'НЕ Сигареты ROTHMANS',
                     'DEMI': 'Сигареты ROTHMANS DEMI',
                     'Aero Blue': 'Сигареты ROTHMANS DEMI CLICK (кнопка / капсула) Aero Blue (фиолетовый / сиреневый / ягоды)',
                     'Plus Blue': 'Сигареты ROTHMANS DEMI CLICK (кнопка / капсула) Plus Blue (amber, мандарин, оранж, апельсин)',
                     'DEMI CLICK': 'Сигареты ROTHMANS DEMI CLICK (кнопка / капсула / зеленый / лайм / ментол)',
                     'DEMI SILVER': 'Сигареты ROTHMANS DEMI SILVER (серый)',
                     'ROYALS BY': 'Сигареты ROYALS BY ROTHMANS',
                     'WRONG CHECK': 'Сигареты ROTHMANS (неизвестно или нет в списке)'}

def predict(texts):
    y = model_M1.predict(texts)
    ind = np.where(y!='NOT')[0]
    y[ind] = model_M2.predict(texts[ind])
    return y

def predict_table(fn_in='../test_dataset.txt', fn_out = '../classified_dataset.txt'):
    df = pd.read_csv(fn_in, sep = '\t')

    df['result'] = predict(df['name'].to_numpy())
    df['result'] = df['result'].apply(lambda x: rename_dict_inverse[x])
    df.to_csv(fn_out, sep='\t', index = None)
    return
